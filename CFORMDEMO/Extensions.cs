﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace CFORMDEMO
{
    static class Extensions
    {
        private const int ADJUST_TO_FIT = -1;

        public static T GetValue<T>(this ListView listView, int index)
        {
            return (T)listView.Items[index].Tag;
        }

        public static T GetSelectedValue<T>(this ListView listView)
        {
            if (listView.SelectedIndices.Count == 0)
                throw new ArgumentException("No item selected");

            return (T)listView.Items[listView.SelectedIndices[0]].Tag;
        }

        public static void AutoSizeColumns(this ListView listView)
        {
            foreach (ColumnHeader column in listView.Columns)
            {
                column.Width = ADJUST_TO_FIT;
            }
        }
    }
}
