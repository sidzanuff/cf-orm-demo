﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using OpenNETCF.ORM;
using CFORMDEMO.Model;

namespace CFORMDEMO
{
    class DataStoreHelper
    {
        private const string DATABASE_FILENAME = "books.db";

        public static IDataStore GetDataStore()
        {
            IDataStore store = new SQLiteDataStore(DATABASE_FILENAME);

            store.AddType(typeof(Book));
            store.AddType(typeof(Author));

            if (!store.StoreExists)
            {
                store.CreateStore();
            }

            return store;
        }
    }
}
