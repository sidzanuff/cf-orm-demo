﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using OpenNETCF.ORM;

namespace CFORMDEMO.Model
{
    [Entity(KeyScheme = KeyScheme.Identity)]
    public class Author
    {
        [Field(IsPrimaryKey = true)]
        public int AuthorID { get; set; }

        [Reference(typeof(Book), "AuthorID", Autofill = false)]
        public Book[] Books { get; set; }

        [Field(SearchOrder = FieldSearchOrder.Ascending)]
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
