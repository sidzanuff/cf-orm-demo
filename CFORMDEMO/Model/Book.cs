﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using OpenNETCF.ORM;

namespace CFORMDEMO.Model
{
    public enum BookType : int
    {
        Fiction = 0,
        NonFiction = 1
    }

    [Entity(KeyScheme = KeyScheme.Identity)]
    public class Book
    {
        [Field(IsPrimaryKey = true)]
        public int BookID { get; set; }

        [Field]
        public int AuthorID { get; set; }

        [Field]
        public string Title { get; set; }

        [Field(SearchOrder = FieldSearchOrder.Ascending)]
        public BookType BookType { get; set; }

    }
}
