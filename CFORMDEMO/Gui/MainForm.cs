﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenNETCF.ORM;
using CFORMDEMO.Model;
using System.Diagnostics;

namespace CFORMDEMO.Gui
{
    public partial class MainForm : Form
    {
        private const int TEST_ENTRIES_COUNT = 100;

        private IDataStore store;

        public MainForm()
        {
            InitializeComponent();
            store = DataStoreHelper.GetDataStore();
        }

        private void authorsButton_Click(object sender, EventArgs e)
        {
            new AuthorListForm().ShowDialog();
        }

        private void booksButton_Click(object sender, EventArgs e)
        {
            new BookListForm().ShowDialog();
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            long timeWithoutTransaction = CalculateInsertTimeWithoutTransaction();
            long timeWithTransaction = CalcluateInsertTimeWithTransaction();

            DeleteEmptyAuthors();

            Cursor.Current = Cursors.Default;

            MessageBox.Show(string.Format(
                "Time without transaction: {0} ms\nTime with transaction: {1} ms", 
                timeWithoutTransaction, timeWithTransaction));

        }

        private long CalculateInsertTimeWithoutTransaction()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            InsertAuthors();
            stopWatch.Stop();
            return stopWatch.ElapsedMilliseconds;
        }

        private long CalcluateInsertTimeWithTransaction()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            store.BeginTransaction();
            InsertAuthors();
            store.Commit();
            stopWatch.Stop();
            return stopWatch.ElapsedMilliseconds;
        }

        private void InsertAuthors()
        {
            for (int i = 0; i < TEST_ENTRIES_COUNT; i++)
            {
                store.Insert(new Author());
            }
        }

        private void DeleteEmptyAuthors()
        {
            IEnumerable<Author> authors = store.Select<Author>(a => string.IsNullOrEmpty(a.Name));
            store.BeginTransaction();
            authors.ToList().ForEach(a => store.Delete(a));
            store.Commit();
        }
    }
}