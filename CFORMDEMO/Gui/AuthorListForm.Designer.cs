﻿namespace CFORMDEMO.Gui
{
    partial class AuthorListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.addMenuItem = new System.Windows.Forms.MenuItem();
            this.EditMenuItem = new System.Windows.Forms.MenuItem();
            this.DeleteMenuItem = new System.Windows.Forms.MenuItem();
            this.authorList = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            // 
            // menuItem1
            // 
            this.menuItem1.MenuItems.Add(this.addMenuItem);
            this.menuItem1.MenuItems.Add(this.EditMenuItem);
            this.menuItem1.MenuItems.Add(this.DeleteMenuItem);
            this.menuItem1.Text = "File";
            // 
            // addMenuItem
            // 
            this.addMenuItem.Text = "Add";
            this.addMenuItem.Click += new System.EventHandler(this.addMenuItem_Click);
            // 
            // EditMenuItem
            // 
            this.EditMenuItem.Enabled = false;
            this.EditMenuItem.Text = "Edit";
            this.EditMenuItem.Click += new System.EventHandler(this.editMenuItem_Click);
            // 
            // DeleteMenuItem
            // 
            this.DeleteMenuItem.Enabled = false;
            this.DeleteMenuItem.Text = "Delete";
            this.DeleteMenuItem.Click += new System.EventHandler(this.deleteMenuItem_Click);
            // 
            // authorList
            // 
            this.authorList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.authorList.Location = new System.Drawing.Point(0, 0);
            this.authorList.Name = "authorList";
            this.authorList.Size = new System.Drawing.Size(638, 450);
            this.authorList.TabIndex = 0;
            this.authorList.SelectedIndexChanged += new System.EventHandler(this.authorList_SelectedIndexChanged);
            // 
            // AuthorListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(638, 455);
            this.Controls.Add(this.authorList);
            this.Menu = this.mainMenu1;
            this.Name = "AuthorListForm";
            this.Text = "Authors";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox authorList;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem addMenuItem;
        private System.Windows.Forms.MenuItem EditMenuItem;
        private System.Windows.Forms.MenuItem DeleteMenuItem;

    }
}

