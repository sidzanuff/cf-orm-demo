﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenNETCF.ORM;
using CFORMDEMO.Model;

namespace CFORMDEMO.Gui
{
    public partial class BookForm : Form
    {
        public BookForm()
        {
            InitializeComponent();

            FillAuthorsComboBox();
            FillTypesComboBox();
        }

        private void BookForm_Load(object sender, EventArgs e)
        {
            titleTextBox.SelectionLength = titleTextBox.Text.Length;
        }

        private void FillAuthorsComboBox()
        {
            IDataStore store = DataStoreHelper.GetDataStore();
            IEnumerable<Author> authors = store.Select<Author>();
            authors.ToList().ForEach(a => authorComboBox.Items.Add(a));
            authorComboBox.SelectedIndex = 0;
        }

        private void FillTypesComboBox()
        {
            typeComboBox.Items.Add("Fiction");
            typeComboBox.Items.Add("NonFiction");
            typeComboBox.SelectedIndex = 0;
        }

        public int AuthorID
        {
            get { return ((Author)authorComboBox.SelectedItem).AuthorID; }

            set
            {
                foreach (Author author in authorComboBox.Items)
                {
                    if (author.AuthorID == value)
                    {
                        authorComboBox.SelectedItem = author;
                        break;
                    }
                }
            }
        }

        public string Title
        {
            get { return titleTextBox.Text; }
            set { titleTextBox.Text = value; }
        }

        public BookType BookType
        {
            get { return (BookType)typeComboBox.SelectedIndex;}
            set { typeComboBox.SelectedIndex = (int)value;}
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}