﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenNETCF.ORM;
using CFORMDEMO.Model;

namespace CFORMDEMO.Gui
{
    public partial class BookListForm : Form
    {
        private IDataStore store;

        public BookListForm()
        {
            InitializeComponent();
            store = DataStoreHelper.GetDataStore();
            RefreshList();
        }

        private void RefreshList()
        {
            bookList.Items.Clear();

            IEnumerable<Author> authors = store.Select<Author>(true);

            foreach (var author in authors)
            {
                foreach (var book in author.Books)
                {
                    ListViewItem item = new ListViewItem(new string[] {
                        author.Name, book.Title, GetBookTypeString(book.BookType)});
                    item.Tag = book;

                    bookList.Items.Add(item);
                }
            }

            bookList.AutoSizeColumns();
        }

        private static string GetBookTypeString(BookType bookType)
        {
            switch (bookType)
            {
                case BookType.Fiction:
                    return "Fiction";
                case BookType.NonFiction:
                    return "NonFiction";
                default:
                    return "";
            }
        }

        private void addMenuItem_Click(object sender, EventArgs e)
        {
            if (store.Count<Author>() == 0)
            {
                MessageBox.Show("No authors added to the database!");
                return;
            }

            BookForm bookForm = new BookForm();

            if (bookForm.ShowDialog() == DialogResult.OK)
            {
                Book book = new Book();

                book.AuthorID = bookForm.AuthorID;
                book.BookType = bookForm.BookType;
                book.Title = bookForm.Title;

                store.Insert(book);

                RefreshList();
            }
        }

        private void bookList_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool selected = bookList.SelectedIndices.Count == 1;

            editMenuItem.Enabled = selected;
            deleteMenuItem.Enabled = selected;
        }

        private void editMenuItem_Click(object sender, EventArgs e)
        {
            Book book = bookList.GetSelectedValue<Book>();
            
            BookForm bookForm = new BookForm();

            bookForm.AuthorID = book.AuthorID;
            bookForm.BookType = book.BookType;
            bookForm.Title = book.Title;

            if (bookForm.ShowDialog() == DialogResult.OK)
            {
                book.AuthorID = bookForm.AuthorID;
                book.BookType = bookForm.BookType;
                book.Title = bookForm.Title;

                store.Update(book);

                RefreshList();
            }
        }

        private void deleteMenuItem_Click(object sender, EventArgs e)
        {
            Book book = bookList.GetSelectedValue<Book>();
            store.Delete(book);
            RefreshList();
        }
    }
}