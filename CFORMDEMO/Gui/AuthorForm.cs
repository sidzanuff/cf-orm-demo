﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CFORMDEMO.Gui
{
    public partial class AuthorForm : Form
    {
        public AuthorForm()
        {
            InitializeComponent();
        }

        public string AuthorName
        {
            get { return nameTextBox.Text; }
            set { nameTextBox.Text = value; }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void AuthorForm_Load(object sender, EventArgs e)
        {
            nameTextBox.SelectionLength = nameTextBox.Text.Length;
        }
    }
}