﻿namespace CFORMDEMO.Gui
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.authorsButton = new System.Windows.Forms.Button();
            this.booksButton = new System.Windows.Forms.Button();
            this.testButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // authorsButton
            // 
            this.authorsButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.authorsButton.Location = new System.Drawing.Point(3, 3);
            this.authorsButton.Name = "authorsButton";
            this.authorsButton.Size = new System.Drawing.Size(632, 60);
            this.authorsButton.TabIndex = 0;
            this.authorsButton.Text = "Authors";
            this.authorsButton.Click += new System.EventHandler(this.authorsButton_Click);
            // 
            // booksButton
            // 
            this.booksButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.booksButton.Location = new System.Drawing.Point(3, 69);
            this.booksButton.Name = "booksButton";
            this.booksButton.Size = new System.Drawing.Size(632, 60);
            this.booksButton.TabIndex = 1;
            this.booksButton.Text = "Books";
            this.booksButton.Click += new System.EventHandler(this.booksButton_Click);
            // 
            // testButton
            // 
            this.testButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.testButton.Location = new System.Drawing.Point(3, 135);
            this.testButton.Name = "testButton";
            this.testButton.Size = new System.Drawing.Size(632, 60);
            this.testButton.TabIndex = 2;
            this.testButton.Text = "Transaction test";
            this.testButton.Click += new System.EventHandler(this.testButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(638, 455);
            this.Controls.Add(this.testButton);
            this.Controls.Add(this.booksButton);
            this.Controls.Add(this.authorsButton);
            this.Name = "MainForm";
            this.Text = "CF ORM TEST";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button authorsButton;
        private System.Windows.Forms.Button booksButton;
        private System.Windows.Forms.Button testButton;
    }
}