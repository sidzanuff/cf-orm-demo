﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenNETCF.ORM;
using CFORMDEMO.Model;
using System.Diagnostics;

namespace CFORMDEMO.Gui
{
    public partial class AuthorListForm : Form
    {
        private IDataStore store;

        public AuthorListForm()
        {
            InitializeComponent();

            store = DataStoreHelper.GetDataStore();

            RefreshList();
        }

        private void RefreshList()
        {
            authorList.Items.Clear();

            IEnumerable<Author> authors = store.Select<Author>();
            authors.ToList().ForEach(a => authorList.Items.Add(a));

            RefreshMenu();
        }

        private void RefreshMenu()
        {
            bool selected = authorList.SelectedItem != null;

            EditMenuItem.Enabled = selected;
            DeleteMenuItem.Enabled = selected;
        }

        private void addMenuItem_Click(object sender, EventArgs e)
        {
            AuthorForm authorForm = new AuthorForm();

            if (authorForm.ShowDialog() == DialogResult.OK)
            {
                Author author = new Author { Name = authorForm.AuthorName };
                store.Insert(author);

                RefreshList();
            }
        }

        private void authorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshMenu();
        }

        private void editMenuItem_Click(object sender, EventArgs e)
        {
            AuthorForm authorForm = new AuthorForm();
            Author author = (Author)authorList.SelectedItem;
            authorForm.AuthorName = author.Name;
            
            if (authorForm.ShowDialog() == DialogResult.OK)
            {
                author.Name = authorForm.AuthorName;
                store.Update(author);

                RefreshList();
            }
        }

        private void deleteMenuItem_Click(object sender, EventArgs e)
        {
            Author author = (Author)authorList.SelectedItem;
            store.Delete("Book", "AuthorID", author.AuthorID);
            store.Delete(author);
            RefreshList();
        }
    }
}